Dir.glob("gemfiles/rails_*.gemfile.lock").each { |f| File.delete(f) }

require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
